---
title: 'About'
date: 2018-12-06T09:29:16+10:00
type: 'about'
heroHeading: 'About Us'
heroSubHeading: "We are a team of professionals with decades of enteprise-grade application and technology software development experience"
heroBackground: 'images/banner-about.jpeg'
---

At Senofi, business software is our passion and in the last few years we realized that the Distributed Ledger Technology (DLT) and applications have vast capabilities to improve and revolutionize the business world. In 2018 we founded the company [Senofi](https://senofi.ca) as we acknowledged the value DLT applications can bring to the business. Our focus is on private, permissioned DLT that we believe is the core technology that runs the business transactions in the realm of the distributed business networks. Our goal is to educate our clients as to the benefits of the technology as well as simplify the complexity by introducing our flagship product [Consortia](https://www.consortia.io).
