---
title: 'DLT Engineer'
date: 2020-07-11T12:33:46-04:00
draft: false
featured: true
weight: 1
heroHeading: 'DLT Engineer'
heroBackground: 'images/banner-careers.jpeg'
button: 'View & Apply'
buttonLink: '/careers/dlt-engineer'
---

Are you looking for the next big thing in your career? Thanks for stopping by and checking us out!

### Required Technical Expertise

* experience developing DLT applications or proof-of-concepts (POCs) using Hyperledger Fabric or other DLT platforms
* experience with development process
* experience as a full stack developer with experience in JavaScript, Angular, Node, HTML5, CSS, JSON and one or more of Go/Python/C/C++/Java languages
* experience in an industry developing software
* experience developing prototypes and proof of concept
* experience developing distributed applications and API integration
* experience using Git
* experience working in an Agile environment
* experience in database technologies (e.g. CouchDB/SQL)
* experience with cryptography and public key infrastructure (PKI)

### Education

BS in Computer Science or related degree.

### Job Type

Contract / Full time

If you are interested send us your resume at [careers@senofi.ca](mailto:careers@senofi.ca?subject=DLT%20Engineer).

[<< Back to Careers](/careers/)