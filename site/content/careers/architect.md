---
title: 'Solutions Architect'
date: 2020-07-11T12:33:46-04:00
draft: false
featured: true
weight: 1
heroHeading: 'Solutions Architect'
heroBackground: 'images/banner-careers.jpeg'
button: 'View & Apply'
buttonLink: '/careers/architect'
---

Are you looking for the next big thing in your career? Thanks for stopping by and checking us out!

### What we are looking for

* Someone who would be comfortable having a few beers with us. Not necessarily a beer though, a coffee would do too.
* A superman with some technical skills. Ability to dribble with a soccer ball is a huge plus.
* Someone who has done scubadiving - we sometimes work under pressure so that would probably be useful.


### But seriously...

Not that we weren't serious above but... we would love someone who also has knowledge and interest in the following:

* DLT technologies (buying/selling crypto does not apply)
* Programming languages and when to use one or the other (Go, Java, JavaScript)
* How software is built with cutting-edge technologies
* How to build a solution in the most efficient and cost-effective way
* At least 5 years in an architect role and experience with larger scale projects

### Required Technical Expertise

* experience developing DLT applications or proof-of-concepts (POCs) using Hyperledger Fabric or other DLT platforms
* experience with development process
* experience as a full stack developer with experience in JavaScript, Angular/Vue/React, Node, HTML5, CSS, JSON and one or more of Go/Python/C/C++/Java languages
* experience in an industry developing software
* experience developing prototypes and proof of concept
* experience developing distributed applications and API integration
* experience using Git
* experience working in an Agile environment
* experience in database technologies (e.g. CouchDB/SQL)
* experience with cryptography and public key infrastructure (PKI)

### Education

BS in Computer Science or related degree.

### Job Type

Contract / Full time


If you are interested send us your resume at [careers@senofi.ca](mailto:careers@senofi.ca?subject=Solutions%20Architect).

[<< Back to Careers](/careers/)