---
title: 'Consortia'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-1.png'
draft: false
featured: true
weight: 1
heroHeading: 'Consortia'
heroSubHeading: 'Enlarge your business network by building trust'
heroBackground: 'images/banner-products-consortia.jpeg'
image: 'images/network.png'
summaryText: 'Does your business need a reliable platform to allow for increased trust and transparency? Consortia allows your business to share public and private data with your business network.'
button: 'Discover Consortia'
buttonLink: '/products/consortia'
productButtonLink: 'https://www.consortia.io'
productButton: 'Get Started Now'
---

Does your business need a reliable platform to allow for increased trust and transparency? [Consortia](https://www.consortia.io) allows your business to share public and private data with your business network.

[Consortia](https://www.consortia.io) is a SaaS (Software as a Service) product for deploying and managing DLT business networks and applications based on the enterprise grade open source software technology [Hyperledger Fabric](https://www.hyperledger.org/).


### Features

* Open DLT based business consortium registry. Consortia.io is open to integrate with any other provider.
* Open and simple DLT business network governance. It is easy, simple and highly secure to manage the topology of a business network with your partners.
* Self-managed business networks and applications.
* Work with your business partners in a near real-time approach. Every transaction in the system is recorded based on your predefined rules.
* Maintain a set of immutable transactions and history of all transactions.
* Choose who to share it with and when.

### Why Consortia?

* Ease the adoption of high value DLT business networks and applications by providing a simple way to deploy and manage those networks and applications
* Open registry of business consortiums for easy forming and joining business networks
* Reduce the cost of implementing business applications and managing business networks
* Automate and significantly reduce the DevOps cost
* Simplify and abstract the technical complexity and hence allow businesses to benefit at low cost
* Cross cloud deployments and scaling
* Cloud agnostic with full control over business data without being locked into a single cloud provider
* Keep your transactional data on your own infrastructure
* Full control over your business network nodes and operations (with or without Consortia)

### Who is Consortia for?

* Businesses with a need of enterprise-grade application deployments across clouds with an out of the box business application deployments
* Business application software vendors
* Software service vendors
* Business application developers
