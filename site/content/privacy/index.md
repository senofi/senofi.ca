---
title: "Privacy Policy"
date: 2020-07-01
type: 'privacy'
---

This Privacy Policy sets out how Senofi Inc. (“Senofi”, “we” or “us”) collects, uses and discloses information about identifiable individuals and information which can be used to identify an individual (“Personal Information”) through the Senofi website located at [Insert URL] (the “Website”).  The Website provides users with a platform that allows individuals and organizations to create secure networks with other user groups to collaborate through the Website (the “Service”). Terms not defined herein shall have the meaning as set out in Senofi’s Terms of Use. 
The privacy of our users is of great importance to us. By visiting our Website or using the Service in any manner, you acknowledge that you accept the practices and policies outlined in this Privacy Policy and you hereby consent to the collection, use and disclosure of your Personal Information in accordance with this Privacy Policy. This Privacy Policy does not apply to the practices of companies that we do not own or control.
### 1. Website Visitors
Senofi may collect information that web browsers and servers typically make available, such as the browser type, language preference, referring website, and the date and time of each visitor request. Senofi may also collect Internet Protocol (IP) addresses for logged in users on the Website. 
Senofi’s purpose in collecting this information is to better understand how our users use the Website. From time to time, Senofi may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its Website, and share such information with Senofi’s agents, partners and affiliates.
### 2. Collection and Use of Personal Information 
Users may be required to register for an account through the Website (“Account”). When you register for the Service, Senofi collects certain information from you (collectively, “Account Information”). In particular, Senofi may collect your personal name, email address, and other authentication information as permitted by applicable laws.

Senofi uses the Account Information to:

* authenticate access to the Account;  
* provide, operate, maintain and improve the Service;  
* respond to comments, questions, and requests;  
* communicate with you about news or information about us and our partners;  
* investigate and prevent unauthorized access to, or use of, the Service and other illegal activities;  
* monitor and analyze trends, usage, and activities in connection with the Website; and  
* for other purposes, which we will notify you about and seek your consent.  

Senofi may also collect certain information from other visitors of the Website, such as Internet addresses, time spent on the Website and other usage data (“Usage Data”). This Usage Data is logged to help diagnose technical problems, and to administer our Website in order to constantly improve the quality of the Service. If you have an Account, this Usage Data is linked to your Account. If you do not have an Account, this Usage Data may be linked to your device ID; however, we only use your device ID if necessary to help us diagnose and/or respond to any issues with the Service.

### 3. Protection of Personal Information
Senofi discloses Personal Information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Senofi’s behalf or to provide the Service, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using the Website, you consent to the transfer of such information to Canada and the United States. 
Other than to its employees, contractors and affiliated organizations, as described above, Senofi discloses Personal Information only in response to a subpoena, court order or other governmental request, or when Senofi believes in good faith that disclosure is reasonably necessary to protect the property or rights of Senofi, third parties or the public at large. If you are a registered user of the Website and have supplied your email address, Senofi may occasionally send you an email to solicit your feedback or just keep you up to date with what’s going on with us. Senofi takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of Personal Information.

### 4. Cookies
A cookie is a string of information that a website stores on a visitor’s computer, and that the visitor’s browser provides to the website each time the visitor returns. Senofi may use cookies to help us identify and track visitors, their usage of the Website, and their website access preferences. Users who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using the Website, with the drawback that certain features of the Website may not function properly without the aid of cookies.
Senofi may also use technologies such as beacons, scripts, and tags. These technologies may be used for analyzing trends, administering the Website, tracking users’ movements around the Website, and gathering demographic information about our user base as a whole. Various browsers may offer their own management tools for removing these types of tracking technologies.

### 5. Storage Location and Transfer of Personal Information
Senofi processes and stores its data, including Personal Information, on servers located in the United States. Senofi also transfers data to the third party service providers described on our Sub-Processors webpage located at [Include Sub-Processor Domain Name] (“Sub-Processors”). By submitting Personal Information or otherwise using the Service, you agree to this transfer, storing or processing of your Personal Information in the United States. You acknowledge and agree that your Personal Information may be accessible to law enforcement and governmental agencies in the United States under lawful access regimes or court order. 

### 6. Disclosure of Information with Third Parties
We may from time to time employ third parties to perform tasks on our behalf and we may need to share Account Information and other Personal Information with them to provide certain services. Unless we tell you differently, such third parties do not have any right to use the Personal Information we share with them beyond what is necessary for them to provide the tasks and services on our behalf. The third parties we currently engage includes third party companies and individuals employed by us to facilitate the Service, including the provision of database management, payment processing and customer relationship management tools, including the Sub-Processors.

### 7. Business Transfers
If we are acquired by a third party, Personal Information may be made available or otherwise transferred to the new controlling entity, where permitted under applicable law. You acknowledge that such transfers may occur, and that any acquirer of Senofi may continue to use your personal information as set forth in this Privacy Policy.

### 8. Disclosures Permitted Under Law
We will not divulge your Personal Information to non-affiliated third parties without your consent, except in the following limited circumstances:  
* to the extent required to comply with any legal or regulatory obligation or when we are compelled to do so by a governmental agency, court or other entity;  
* to transfer or otherwise disclose information to third parties who perform business, professional and/or technical functions (“Operational Service Providers”) for us related to the purpose for which you disclosed the Personal Information in accordance with any applicable legal requirements; and  
* in the event we believe your actions violate any law, regulation, or if you threaten the rights, property, safety of us, our subsidiaries or affiliated companies, our Service, or any of our Operational Service Providers.  

### 9. How Your Information is Protected
Senofi maintains reasonable standards of security and confidentiality consistent with customary business practice to protect the information under our control from loss, misuse, and alteration. However, for most internet websites, it is possible that third parties may unlawfully intercept or access transmissions over an unsecured transmission. We also limit access to our Website and Service by our own employees and contractors to individuals who are authorized for the proper handling of such information and any employee found violating our standards of security and confidentiality will be subject to our disciplinary process.

### 10. Third Party Websites
The Website may provide links to third party websites. Please note that Senofi does not control these websites and resources. As such we are not responsible for their availability, content, or delivery of services. These other websites are not bound to Senofi’s Privacy Policy. They may have their own policies or none at all. We encourage you to review the privacy policies of these third party websites. Senofi is not responsible for the privacy practices or content of third party websites.

### 11. Access and Accuracy 

You have the right to access the Personal Information we hold about you in order to verify the Personal Information we have collected in respect to you and to have a general account of our uses of that information.  Upon receipt of your written request, we will provide you with a copy of your Personal Information, although in certain limited circumstances, and as permitted under law, we may not be able to make all relevant information available to you, such as where that information also pertains to another user.  In such circumstances we will provide reasons for the denial to you upon request.  We will endeavor to deal with all requests for access and modifications in a timely manner.
We will make every reasonable effort to keep your Personal Information accurate and up to date, and we will provide you with mechanisms to update, correct, delete or add to your Personal Information as appropriate.  As appropriate, this amended Personal Information will be transmitted to those parties to which we are permitted to disclose your information.  Having accurate Personal Information about you enables us to give you the best possible service.
### 12. Residents of the European Economic Area
If you are a resident of the European Economic Area (“EEA”), you have certain data protection rights. Senofi aims to take reasonable steps to allow you to correct, amend, delete, or limit the use of your Personal Information (known as “Personal Data” under the EU General Data Protection Regulation).
If you wish to be informed what Personal Data we hold about you and if you want it to be removed from our systems, please contact us using the contact information set out below.
In certain circumstances, where we act as data controller, you have the following data protection rights:  

* Request access to your Personal Data (commonly known as a "data subject access request"). This enables you to receive a copy of the Personal Information we hold about you where we are the data controller and to check that we are lawfully processing it.  
* Request correction of the Personal Data that we hold about you. This enables you to have any incomplete or inaccurate information we hold about you corrected, though we may need to verify the accuracy of the new information you provide to us.  
* Request erasure of your Personal Data. This enables you to ask us to delete or remove Personal Information where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your Personal Information where you have successfully exercised your right to object to processing (see below), where we may have processed your information unlawfully, or where we are required to erase your Personal Information to comply with local law. Note, however, that we may not always be able to comply with your request of erasure for specific legal reasons which will be notified to you, if applicable, at the time of your request.  
* Object to processing of your Personal Data where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground as you feel it impacts on your fundamental rights and freedoms. You also have the right to object where we are processing your Personal Information for direct marketing purposes. In some cases, we may demonstrate that we have compelling legitimate grounds to process your information which override your rights and freedoms.  
* Request restriction of processing of your Personal Data. This enables you to ask us to suspend the processing of your Personal Data in the following scenarios: ( a ) if you want us to establish the information's accuracy; ( b ) where our use of the information is unlawful but you do not want us to erase it; ( c ) where you need us to hold the information even if we no longer require it as you need it to establish, exercise or defend legal claims; or ( d ) you have objected to our use of your information but we need to verify whether we have overriding legitimate grounds to use it.  
* Request the transfer of your Personal Data to you or to a third party. We will provide to you, or a third party you have chosen, your Personal Information in a structured, commonly used, machine-readable format. Note that this right only applies to automated information which you initially provided consent for us to use or where we used the information to perform a contract with you.  
* Withdraw consent at any time where we are relying on consent to process your Personal Data. However, this will not affect the lawfulness of any processing carried out before you withdraw your consent. If you withdraw your consent, we may not be able to provide certain services to you. We will advise you if this is the case at the time you withdraw your consent.  

Please note that we may ask you to verify your identity before responding to such requests.
You have the right to complain to a Data Protection Authority about our collection and use of your Personal Data. For more information, please contact your local data protection authority in the EEA.

### 13. International Users
If you are visiting the Website or using the Service from outside of Canada, please note that you are agreeing to the transfer of your information to Canada and the United States and for it to be processed globally by our third party service providers. By providing your information, you consent to any transfer and processing in accordance with this Privacy Policy.

### 14. Access, Correction and Accuracy
You have the right to access the Personal Information we hold about you in order to verify the Personal Information we have collected in respect to you and to have a general account of our uses of that information. Upon receipt of your written request, we will provide you with a copy of your Personal Information, although in certain limited circumstances, and as permitted under law, we may not be able to make all relevant information available to you, such as where that information also pertains to another user. In such circumstances we will provide reasons for the denial to you upon request. We will endeavor to deal with all requests for access and modifications in a timely manner.
We will make every reasonable effort to keep your Personal Information accurate and up to date, and we will provide you with mechanisms to update, correct, delete or add to your Personal Information as appropriate. As appropriate, this amended Personal Information will be transmitted to those parties to which we are permitted to disclose your information. Having accurate Personal Information about you enables us to give you the best possible service.

### 15. Changes to this Privacy Policy
We may amend this Privacy Policy from time to time. Use of Personal Information we collect is subject to the Privacy Policy in effect at the time such information is collected, used or disclosed. If we make material changes or changes in the way we use Personal Information, we will notify you by posting an announcement on our Website or sending you an email prior to the change becoming effective. You are bound by any changes to the Privacy Policy when you use the Website or Service after such changes have been first posted.

### 16. Contact
If you have any questions about this Privacy Policy or Senofi’s privacy practices, please contact us at privacy@senofi.ca.

&nbsp;  
Last Updated: 1 July, 2020  

## Sub-Processors

To support Senofi in delivering the Service, Senofi engages third-party service providers as sub-processors. 
This page identifies our sub-processors, describes where they are located, lists what services they provide to us and identifies the type of personal information processed.
Our business needs may change from time to time and Senofi will periodically update this page to provide notice of additions and removals to our list of sub-processors.


| Service Provider | Types of PI processed | Processor Activities | Location |
|---|---|---|---|
| Stripe | Name, billing address, and payment information | Payment processing | United States |
| HubSpot | Name and email address | Customer relationship management | United States | 
| Google Analytics | IP address, location, date and time visited our Website, and amount of time spent on our Website | Analytics | United States |
| Send Grid | Email address | Marketing and transactional email delivery | United States |

&nbsp;  
&nbsp;  
Last Updated: 1 July, 2020