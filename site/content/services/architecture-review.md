---
title: 'Architecture Review'
date: 2018-11-28T15:14:39+10:00
icon: 'services/service-icon-2.png'
featured: true
draft: false
heroHeading: 'Architecture Review'
heroSubHeading: 'Expert advice and guidance when you need it most.'
heroBackground: 'services/service1.jpg'
---

You already have a solution or an idea for a solution and you want a second opinion. We could be of help by collaborating with your in-house team or implementation partner exclusively.

### Our Focus

During Architecture Review engagements our focus falls on but is not limited to:

* Improving the project quality
* Applying best practices in software development, project management, testing and maintenance
* Validating the approach and suggesting improvements that would make significant impact on your project

### Methodology

When it comes to Architecture Review we use the following process:

* Understand your drive, solution and expectations
* Work with project stakeholders to identify functional and non-functional requirements
* Conduct interviews with the technical team to gather all the ins-and-outs of the projected, or already built solution
* Identify pain points and group and sort them by severity, priority and business impact
* Present the findings in a document and in-persion/remote presentation
* Work on a plan together with the team to address the outlined problems and improvements based on the team's feedback


[Get in touch with us](/contact/)

[<< Back to Services](/services/)